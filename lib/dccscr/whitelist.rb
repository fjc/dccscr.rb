# frozen_string_literal: true

require 'json'
require 'yaml'
require 'tmpdir'
require 'shellwords'

module DCCSCR
  # Class to download the dccscr_whitelist repo and store greylist entries.
  #
  class Whitelist
    UPSTREAM_REPO = 'https://repo1.dso.mil/dsop/dccscr-whitelists.git'

    attr_reader :path, :repo, :entries

    def initialize(path: nil, repo: nil, clone: true, clone_options: '--depth 1')
      if path
        @path = path
      else
        @path = Dir.mktmpdir
        at_exit { FileUtils.remove_entry @path }
      end

      if repo
        @repo = repo
      else
        @repo = UPSTREAM_REPO
      end

      clone_repo(clone_options) if clone

      @entries = {}
    end

    def [](subpath)
      entries[subpath] ||= Entry.new(whitelist: self, subpath: subpath)
    end

    # Internal class to hold a single greylist.
    #
    class Entry
      attr_reader :value, :parent

      def initialize(whitelist:, subpath:, greylist: "#{File.basename(subpath)}.greylist")
        warn "Parse: #{File.join subpath,  greylist}"

        @value = JSON.parse(File.read(File.join(whitelist.path, subpath, greylist)))

        whitelist[@parent] unless (@parent = @value['image_parent_name'] || '').empty?
      end
    end

    private

    def clone_repo(clone_options = '')
      system Shellwords.join [].tap { |cmd|
        cmd << %w[git clone]
        cmd << Shellwords.split(clone_options).map { |w| Shellwords.escape(w) }
        cmd << ['--', Shellwords.escape(@repo), Shellwords.escape(@path)]
      }.flatten
      $?.success? || fail('error cloning repo')
    end
  end
end
