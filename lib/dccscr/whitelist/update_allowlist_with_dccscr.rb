#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../whitelist'

# Service class to update a GitLab vulnerability-allowlist.yml with
# whitelisted_vulnerabilities from the dccscr-whitelist for a set
# of images.
#
class DCCSCR::Whitelist::UpdateAllowlistWithDCCSCR
  attr_reader :images, :allow_filename, :local_filename

  def initialize(images: [], allow_filename: nil, local_filename: nil)
    @images = images
    @allow_filename = allow_filename || 'vulnerability-allowlist.yml'
    @local_filename = local_filename || 'local-vulnerability-allowlist.yml'
  end

  def whitelist
    @_whitelist ||= DCCSCR::Whitelist.new
  end

  def run
    ll = load_gitlab_allowlist

    wl = load_dccscr_whitelist
    dl = allow_list_dccscr(wl)

    cl = combined_list(dl, ll)

    update_allow_list_file(cl)
  end

  private

  def load_dccscr_whitelist
    whitelist.tap do |wl|
      # load wl entries for args
      # will load parents as well
      images.each { |arg| wl[arg] }
    end
  end

  def load_gitlab_allowlist
    if File.exist?(local_filename)
      warn 'Loading local file'
      load(local_filename)
    elsif File.exist?(allow_filename)
      warn 'Loading and renaming local allow file'
      File.rename(allow_filename, local_filename)
      load(local_filename)
    else
      warn 'No local allow file'
      {}
    end
  end

  def load(yml)
    YAML.safe_load(File.read(yml))
  end

  def allow_list_dccscr(wl)
    warn 'Generating dccscr list in gitlab format'

    {
      'generalallowlist' => Hash[
        wl.entries.map { |_, entry|
          entry.value['whitelisted_vulnerabilities'].map { |v|
            [v['vulnerability'], "dccscr-whitelists:\n#{v['justification']}"]
          }.compact
        }.flatten(1).sort
      ]
    }
  end

  def combined_list(dl, ll)
    warn 'Merging dccscr and local lists'

    dl.merge(ll) { |_, d, l|
      case d
      when Hash
        d.merge(l)
      else
        l
      end
    }
  end

  def update_allow_list_file(cl)
    warn 'Updating allow file'

    File.open(allow_filename, 'w') do |f|
      f << cl.to_yaml
    end
  end
end
