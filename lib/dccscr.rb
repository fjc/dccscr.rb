# frozen_string_literal: true

require_relative 'dccscr/version'

module DCCSCR
  class Error < StandardError; end
  # Your code goes here...
end
