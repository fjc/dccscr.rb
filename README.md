# DCCSCR.rb

Ruby helpers to work with DoD Centralized Container Source Code Repository (DCCSCR):

1. `update_allowlist_with_dccscr` - Convert DoD DCCSCR vulnerability greylists to a GitLab vulnerability allowlist.
2. ???
3. Profit

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'dccscr'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install dccscr

## Usage

### update_allowlist_with_dccscr

Build an allowlist for a base image:

    $ update_allowlist_with_dccscr opensource/ruby/ruby27

Algorithm:

1. `load_gitlab_allowlist`
  * load `local-vulnerability-allowlist.yml`, if any
  * otherwise, load `vulnerability-allowlist.yml` and rename to `local-vulnerability-allowlist.yml`, if any
2. `load_dccscr_whitelist`
  * checkout the `dccscr-whitelists` repository
  * load the entry for each requested image
  * it will also recursively load the parent image for each
3. `allow_list_dccscr`
  * Convert the dccscr whitelist(s) to the gitlab format
4. `combined_list`
  * Merge the converted dccscr list with any loaded local list
5. `update_allow_list_file`
  * Save the result to `vulnerability-allowlist.yml`

Sample `.gitlab-ci.yml` snippet:

```
include:
  - template: Security/Container-Scanning.gitlab-ci.yml

# Extend the container scanning job to use dccscr.rb
container-scanning:
  before_script:
    - gem install dccscr
    - update_allowlist_with_dccscr $DCCSCR_IMAGE_LIST
  variables:
    DCCSCR_IMAGE_LIST: >-
      opensource/ruby/ruby27
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/dccscr.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
