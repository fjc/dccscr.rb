# frozen_string_literal: true

# rubocop:disable Layout/ExtraSpacing
# rubocop:disable Layout/LineLength
# rubocop:disable Metrics/BlockLength
# rubocop:disable Style/ParallelAssignment
# rubocop:disable Style/Semicolon

require 'test_helper'

require 'dccscr'
require 'dccscr/whitelist'
require 'dccscr/whitelist/update_allowlist_with_dccscr'

describe DCCSCR do
  it 'is a module' do
    ok { ::DCCSCR }.is_a? Module
  end

  it 'has a version number' do
    ok { Gem::Version.new(::DCCSCR::VERSION) }.truthy?
  end

  describe DCCSCR::Whitelist do
    it 'is a class' do
      ok { ::DCCSCR::Whitelist }.is_a? Class
    end

    it 'has a default upstream repo' do
      ok { URI.parse(::DCCSCR::Whitelist::UPSTREAM_REPO) }.is_a? URI::HTTP
    end

    describe '#clone_repo' do
      wl = ::DCCSCR::Whitelist.allocate

      it 'runs the git command' do
        wl.instance_variable_set(:@repo, 'repo')
        wl.instance_variable_set(:@path, 'path')

        wl.stub :system, ->(s) { ok { s } == 'git clone -- repo path' } do
          wl.send(:clone_repo)
        end

        wl.stub :system, ->(s) { ok { s } == 'git clone --depth 1 -- repo path' } do
          wl.send(:clone_repo, '--depth 1')
        end
      end

      it 'shell escapes the repo' do
        wl.instance_variable_set(:@path, 'path')
        wl.instance_variable_set(:@repo, 'repo#;&"\\`:!*?$repo')
        wl.stub :system, ->(s) { ok { s } =~ '-- repo\\\\\\#\\\\\\;\\\\\\&\\\\\\"\\\\\\\\\\\\\\`:\\\\\\!\\\\\\*\\\\\\?\\\\\\$repo path' } do
          wl.send(:clone_repo)
        end
      end

      it 'shell escapes the path' do
        wl.instance_variable_set(:@repo, 'repo')
        wl.instance_variable_set(:@path, 'path#;&"\\`:!*?$path')
        wl.stub :system, ->(s) { ok { s } =~ 'repo path\\\\\\#\\\\\\;\\\\\\&\\\\\\"\\\\\\\\\\\\\\`:\\\\\\!\\\\\\*\\\\\\?\\\\\\$path' } do
          wl.send(:clone_repo)
        end
      end

      it 'shell escapes the clone_options' do
        wl.instance_variable_set(:@repo, 'repo')
        wl.instance_variable_set(:@path, 'path')

        wl.stub :system, ->(s) { ok { s } =~ 'clone -- repo' } do
          wl.send(:clone_repo)
        end

        wl.stub :system, ->(s) { ok { s } =~ 'clone one\\\\\\#\\\\\\;\\\\\\&two three:\\\\\\!\\\\\\*\\\\\\?\\\\\\$four --' } do
          wl.send(:clone_repo, 'one#;&\\two three:!*?$four')
        end
      end
    end

    describe DCCSCR::Whitelist::Entry do
      it 'is a class' do
        ok { ::DCCSCR::Whitelist::Entry }.is_a? Class
      end

      it 'has a value and a parent' do
        e = ::DCCSCR::Whitelist::Entry.allocate

        v, p = Hash.allocate, String.allocate

        e.instance_variable_set(:@value,  v)
        e.instance_variable_set(:@parent, p)

        ok { e.value  }.same?(v)
        ok { e.parent }.same?(p)
      end

      it 'parses a JSON file for the value' do
        File.stub :read, ->(fn) { ok { fn }.send :==, 'path/sp/gl'; '{"j":"s","o":"n"}' } do
          wl, sp, gl = ::DCCSCR::Whitelist.allocate, String.new('sp'), String.new('gl')
          wl.instance_variable_set(:@path,  String.new('path'))
          e = ::DCCSCR::Whitelist::Entry.new(whitelist: wl, subpath: sp, greylist: gl)
          ok { e.value } == { 'j' => 's', 'o' => 'n' }
        end
      end

      it 'gets the parent and looks up the parent' do
        File.stub :read, '{"image_parent_name":"ipn"}' do
          wl, sp, gl = ::DCCSCR::Whitelist.allocate, String.new('sp'), String.new('gl')
          wl.instance_variable_set(:@path,  String.new('path'))
          wl.stub :[], ->(ipn) { ok { ipn } == 'ipn' } do
            e = ::DCCSCR::Whitelist::Entry.new(whitelist: wl, subpath: sp, greylist: gl)
            ok { e.parent } == 'ipn'
          end
        end
      end

      it 'loads a default filename based on the subpath' do
        File.stub :read, ->(fn) { ok { fn }.send :==, 'path/sp/sp.greylist'; '{}' } do
          wl, sp = ::DCCSCR::Whitelist.allocate, String.new('sp')
          wl.instance_variable_set(:@path,  String.new('path'))
          ::DCCSCR::Whitelist::Entry.new(whitelist: wl, subpath: sp)
        end
      end
    end

    # class Entry
    #   attr_reader :value, :parent
    #   def initialize(whitelist:, subpath:, greylist: "#{File.basename(subpath)}.greylist")
    #     @value = JSON.parse(File.read(File.join(whitelist.path, subpath, greylist)))
    #     whitelist[@parent] unless (@parent = @value['image_parent_name'] || '').empty?
    #   end
    # end

    describe DCCSCR::Whitelist::UpdateAllowlistWithDCCSCR do
      it 'is a class' do
        ok { ::DCCSCR::Whitelist::UpdateAllowlistWithDCCSCR }.is_a? Class
      end
    end
  end
end

# rubocop:enable Style/Semicolon
# rubocop:enable Style/ParallelAssignment
# rubocop:enable Metrics/BlockLength
# rubocop:enable Layout/LineLength
# rubocop:enable Layout/ExtraSpacing
