# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/ok'

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
